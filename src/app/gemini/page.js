'use client'

import {useState} from "react";
import '../globals.css'
import axios from "axios";

export default function GeminiPage(){
    const [question, setQuestion] = useState( '')
    const [response , setResponse] = useState(["답변 공간 !!"])

    const handleInput = e  =>{
        setQuestion(e.target.value)
    }
    const handleSubmit = e =>{
        // 다른 이벤트 막으려고
        e.preventDefault()
        const apiUrl ='https://generativelanguage.googleapis.com/v1/models/gemini-1.5-flash:generateContent?' +
            'key={본인 key 입력하기}'
        const data =  { "contents":[
        { "parts":[
            {"text": question}]}]}
        axios.post(apiUrl,data)
            .then(res => {
                console.log(res)
                setResponse(res.data.candidates[0].content.parts[0].text)
            }).catch(err => {
                console.error(err)
        })
    }

    return (

        <div>
            <main className="flex min-h-screen flex-col items-center justify-between p-24">
                <form onSubmit={handleSubmit}>
                    <input type="text" value={question} onChange={handleInput} placeholder="질문을 입력하세요"/>
                    <button type="submit"> 입력</button>
                </form>
                {/*and 를 이용한 if 조건문 표현하기 !! */}
                {response && <div> {response} </div>}
            </main>
        </div>
)
}
